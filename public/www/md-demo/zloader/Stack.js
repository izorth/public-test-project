class CustomController extends ZCustomComponent {
    onThis_init() {
        this.demo1.startDemo("md-demo/zloader/StackDemo", [
            {name:"StackDemo.html", path:"md-demo/zloader/StackDemo.html"},
            {name:"StackDemo.js", path:"md-demo/zloader/StackDemo.js"},
            {name:"StackRecord.html", path:"md-demo/zloader/StackRecord.html"},
            {name:"StackRecord.js", path:"md-demo/zloader/StackRecord.js"},
            {name:"StackNameEditor.html", path:"md-demo/zloader/StackNameEditor.html"},
            {name:"StackNameEditor.js", path:"md-demo/zloader/StackNameEditor.js"},
            {name:"StackAddressEditor.html", path:"md-demo/zloader/StackAddressEditor.html"},
            {name:"StackAddressEditor.js", path:"md-demo/zloader/StackAddressEditor.js"}
        ], 300);
    }
}