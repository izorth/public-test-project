class CustomController extends ZCustomComponent {
    onThis_init() {
        this.demo1.startDemo("md-demo/intro/concepts/Demo1", [
            {name:"Demo1.html", path:"md-demo/intro/concepts/Demo1.html"},
            {name:"Demo1.js", path:"md-demo/intro/concepts/Demo1.js"}
        ], 200);
    }
}