class CustomController extends ZCustomComponent {
    onThis_init() {
        this.demo1.startDemo("md-demo/intro/templates/bmd/Main", [
            {name:"index.html", path:"md-demo/intro/templates/bmd/index.html"},
            {name:"Main.html", path:"md-demo/intro/templates/bmd/Main.html"},
            {name:"Main.js", path:"md-demo/intro/templates/bmd/Main.js"}
        ], 500);
    }
}