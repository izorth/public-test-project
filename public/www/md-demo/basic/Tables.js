class CustomController extends ZCustomComponent{
    onThis_init(){
        this.demo1.startDemo("md-demo/basic/tables/Demo1", [
            {name:"Demo1.html", path:"md-demo/basic/tables/Demo1.html"},
            {name:"Edit.html", path:"md-demo/basic/tables/Edit.html"},
            {name:"Delete.html", path:"md-demo/basic/tables/Delete.html"},
            {name:"Demo1.js", path:"md-demo/basic/tables/Demo1.js"},
            {name:"Edit.js", path:"md-demo/basic/tables/Edit.js"},
            {name:"Delete.js", path:"md-demo/basic/tables/Delete.js"}
        ], 300);
    }
}