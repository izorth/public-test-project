class CustomController extends ZCustomComponent{
    onThis_init(){
        this.demo1.startDemo("md-demo/basic/showdialogs/Demo1", [
            {name:"Demo1.html", path:"md-demo/basic/showdialogs/Demo1.html"},
            {name:"Dialog.html", path:"md-demo/basic/showdialogs/Dialog.html"},
            {name:"Demo1.js", path:"md-demo/basic/showdialogs/Demo1.js"},
            {name:"Dialog.js", path:"md-demo/basic/showdialogs/Dialog.js"}
        ], 300);
    }
}