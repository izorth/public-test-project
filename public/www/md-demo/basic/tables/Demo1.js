class CustomController extends ZCustomComponent {
    onThis_init(){
        this.view.bootstrapMaterialDesign();
        this.f=0;    
        this.refresca();
       
    }
    
    refresca() {
        
        this.listaUsuarios.refresh();
        
    }

    prepararFila(row){
        delete row._rowClass;
        return row;
    }

    setDatos(r){
        let d = getDatos();
        if(r!=null){
            d.push(r);
            this.f++
            return d;
        }else{
            return d;
        }
    }
    
    onListaUsuarios_getRows(cb) {
        cb(this.setDatos());
    }
    
    onListaUsuarios_afterPaint() {
    }
    
    onCmdAgregarUsuario_click() {
        this.showDialog("./Edit", {newRecord:true}, usuario =>{
            this.setDatos(this.prepararFila(usuario));
            this.listaUsuarios.updateRow(this.f+1, usuario);
        });
    }
  
    onListaUsuarios_deleteRequest(idx, row) {
        this.showDialog("./Delete", {message:"Do you really want to delete: '"+row.name+"'?"}, ()=>{
            this.listaUsuarios.deleteRow(idx);
        } );
    }

    onListaUsuarios_editRequest(idx, row) {
        this.showDialog("./Edit",  {record:row}, usuario => {
            this.listaUsuarios.updateRow(idx, this.prepararFila(usuario));
        });
        this.refresca();
    }
}

function  getDatos(){
    
    return [{
        id: "0",
        name:"name1",
        lastname: "lastname1",
        email: "email@ejemplo.cl"
    },
    {
        id: "1",
        name: "name2",
        lastname: "lastname2",
        email: "email2@ejemplo.com"
    }];
}