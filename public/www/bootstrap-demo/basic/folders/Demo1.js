class CustomController extends ZCustomComponent {
    startDemo(previewPath, files, editorHeight=200) {
        this.thisId = "demo-" + parseInt(1000000 * Math.random());
        
        // dynamic ids for tab. This component can be repeated in page
        this.tab1.view.attr("href", "#tab1Tab-" + this.thisId);
        this.tab1Tab.view.attr("id", "tab1Tab-" + this.thisId);
        this.tab2.view.attr("href", "#tab2Tab-" + this.thisId);
        this.tab2Tab.view.attr("id", "tab2Tab-" + this.thisId);

        this.tab1Loader.load(previewPath);
        
        this.tab2Loader.load(previewPath);
        
    }
    
    onTab1_click(){
        document.getElementById("tab1Tab").className ="tab-pane fade show active";
        document.getElementById("tab2Tab").className ="tab-pane fade";
        this.tab1Loader.view.text("Content of Tab1");
    }

    onTab2_click(){
        document.getElementById("tab2Tab").className = "tab-pane fade show active";
        document.getElementById("tab1Tab").className ="tab-pane fade";
        this.tab2Loader.view.text("Content of tab2");
    }
}