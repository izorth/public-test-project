const port = 8080;

async function createHTTPServer() {
    let express = require('express');
	let app = express();
    let bodyParser = require('body-parser');
    let http = require('http');    

    app.use("/", express.static(__dirname + "/www"));
    app.use(bodyParser.urlencoded({extended:true}));
	app.use(bodyParser.json());
    httpServer = http.createServer(app);
    httpServer.listen(port, () => console.log("[z-mvc] HTTP Demo Server started on port " + port));
}

createHTTPServer()