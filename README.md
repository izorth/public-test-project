# Zonar Model View Controller

Zonar Model View Controller Framework

This is an interactive platform that allows you to know how make web pages
with the Zonar SPA Framework, which show you the HTML code and the NodeJS code,
so you can made your own web page with this framework.

You can visit this URL: https://izorth.gitlab.io/public-test-project/ .

To contact us, write a mail to contacto@zonar.cl or visit our web page https://web.zonar.cl